verbose = $VERBOSE
$VERBOSE = false

require 'factory_girl'

$VERBOSE = verbose

FactoryGirl.find_definitions

RSpec.configure do |config|
  include FactoryGirl::Syntax::Methods
end
