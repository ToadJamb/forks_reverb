require 'spec_helper'

RSpec.describe Reverb::Response do
  subject { instance }

  let(:instance) { klass.new response }
  let(:response) { instance_double Faraday::Response }
  let(:json_body) { body.to_json }
  let(:body) {{
    'foo' => 'bar',
  }}

  let(:klass) do
    Class.new(described_class) do
      public :etag

      def on_success
        self.data = 'data-value'
      end

      def body_value
        body
      end
    end
  end

  context 'given no response' do
    let(:response) { nil }

    it 'sets the success to false' do
      expect(subject.success?).to eq false
    end
  end

  context 'given a response' do
    before { allow(response).to receive(:body).and_return json_body }
    before { allow(response).to receive(:success?).and_return true }
    before { allow(response).to receive(:status).and_return 200 }

    describe '.new' do
      it 'sets the body to the parsed response body' do
        expect(subject.body_value).to eq({'foo' => 'bar'})
      end
    end

    describe '#data' do
      it 'can be set from a child' do
        expect(subject.data).to eq 'data-value'
      end
    end

    describe '#success?' do
      context 'given a successful response' do
        before { allow(response).to receive(:success?).and_return true }
        it 'returns true' do
          expect(subject.success?).to eq true
        end
      end

      context 'given an unsuccessful response' do
        before { allow(response).to receive(:success?).and_return false }
        before { allow(response).to receive(:body).and_return nil }

        it 'returns false' do
          expect(subject.success?).to eq false
        end

        it 'does not parse the body' do
          expect(subject.body_value).to eq nil
        end
      end
    end

    describe '#status' do
      it 'returns the status from the response' do
        expect(subject.status).to eq 200
      end
    end

    describe '#scrubbed' do
      subject { instance.scrubbed }

      let(:scrub) { true }

      before do
        allow(Reverb::Configuration.current)
          .to receive(:scrub)
          .and_return scrub
      end

      context 'by default' do
        it 'returns a string version of the object' do
          expect(subject).to match(/@body=/)
          expect(subject).to match(/@status=/)
          expect(subject).to match(/@response=/)

          expect(subject).to_not match(/@scrubbed=/)
        end
      end

      context 'given it is not called' do
        it 'is nil' do
          verbose = $VERBOSE
          $VERBOSE = false
          expect(instance.instance_variable_get(:@scrubbed)).to eq nil
          $VERBOSE = verbose
        end
      end

      context 'given the response has etags' do
        let(:etag) { '"asodifja98ya9fehahfaio"' }

        let(:headers) {{ :response_headers => { 'etag' => etag } }}

        let(:response) do
          faraday_response = Faraday::Response.new(headers)
          faraday_response
        end

        it 'scrubs the etags' do
          expect(subject).to match(/"etag".*=>.*"\\"ETAG\\""/)
          expect(subject).to_not match etag
        end

        context 'given scrubbing is turned off' do
          let(:scrub) { false }

          it 'does not scrub the etags' do
            expect(subject).to match etag[1..-2]
            expect(subject).to_not match 'ETAG'
          end
        end

        context 'given scrubbing etags is turned off' do
          before do
            allow(Reverb::Configuration.current)
              .to receive(:scrub_etag)
              .and_return false
          end

          it 'does not scrub the etags' do
            expect(subject).to match etag[1..-2]
            expect(subject).to_not match 'ETAG'
          end
        end
      end
    end

    context '#scrub' do
      let(:scrub) { true }

      before do
        allow(Reverb::Configuration.current)
          .to receive(:scrub)
          .and_return scrub
      end

      shared_context 'a scrubber' do |scrubbed, regex, value, output|
        context "given #{scrubbed.inspect}" do
          before { allow(PP).to receive(:pp).and_return scrubbed }

          before { instance.scrub regex, value }

          context "given #{regex.inspect} is changed to #{value.inspect}" do
            it "sets scrubbed to #{output.inspect}" do
              expect(subject.scrubbed).to eq output
            end

            it 'returns the response' do
              expect(subject).to eq instance
            end

            context 'given scrubbing is disabled' do
              let(:scrub) { false }

              it 'does not change the original string' do
                expect(subject.scrubbed).to eq scrubbed
              end

              it 'returns the response' do
                expect(subject).to eq instance
              end
            end
          end
        end
      end

      it_behaves_like 'a scrubber', 'foobarbaz', /bar/, 'qux', 'fooquxbaz'
      it_behaves_like 'a scrubber', 'foobarbaz', 'bar', 'qux', 'fooquxbaz'
      it_behaves_like 'a scrubber', 'foo/barbaz', /foo\/bar/, 'qux', 'quxbaz'
      it_behaves_like 'a scrubber', 'foo/barbaz', 'foo/bar', 'qux', 'quxbaz'
      it_behaves_like 'a scrubber', 'foobarbaz', nil, 'qux', 'foobarbaz'
      it_behaves_like 'a scrubber', 'foobarbaz', 'bar', nil, 'fooNIL_SCRUBbaz'

      it_behaves_like 'a scrubber',
        'foo%2Fbar/baz%2Fzam', 'foo/bar', 'qux', 'qux/baz%2Fzam'

      it_behaves_like 'a scrubber',
        'foo/bar%20baz/fizz buzz', 'foo/bar baz', 'qux', 'qux/fizz buzz'

      context 'by default' do
        let(:scrubber_regex) { 'scrubber-regex' }
        let(:scrubber_value) { 'scrubber-value' }

        before { subject.scrub scrubber_regex, scrubber_value }

        it 'does not include scrubbers' do
          expect(subject.inspect).to include scrubber_regex
          expect(subject.inspect).to include scrubber_value

          expect(subject.scrubbed).to_not include scrubber_regex
          expect(subject.scrubbed).to_not include scrubber_value
        end
      end
    end

    context '#etag' do
      subject { instance.etag }

      let(:etag) { '"asodifja98ya9fehahfaio"' }

      let(:headers) {{ :response_headers => { 'etag' => etag } }}

      let(:response) do
        faraday_response = Faraday::Response.new(headers)
        faraday_response
      end

      context 'given no .env method on the response' do
        let(:response) { instance_double Faraday::Response }

        before { expect(response).to_not respond_to :env }

        it 'returns nil' do
          expect(subject).to eq nil
        end
      end

      context 'given no response_headers method on response.env' do
        before do
          allow(response)
            .to receive(:env)
            .and_return 'no-env'

          expect(response.env).to_not respond_to :response_headers
        end

        it 'returns nil' do
          expect(subject).to eq nil
        end
      end

      context 'given response_headers is not a hash' do
        let(:headers) {{ :response_headers => ['foo'] }}

        before do
          allow(response.env)
            .to receive(:response_headers)
            .and_return Object.new

          expect(response.env.response_headers).to_not respond_to :[]
          expect(response.env.response_headers).to_not be_a Hash
        end

        it 'returns nil' do
          expect(subject).to eq nil
        end
      end

      context 'given no etag key' do
        let(:headers) {{ :response_headers => { 'foo' => 'bar' } }}
        it 'returns nil' do
          expect(subject).to eq nil
        end
      end

      context 'given the etag is nil' do
        let(:etag) { nil }
        it 'returns nil' do
          expect(subject).to eq nil
        end
      end
    end
  end
end
