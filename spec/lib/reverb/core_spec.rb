require 'spec_helper'

RSpec.describe Reverb::Core do
  describe '.configuration' do
    let(:config) { Struct.new(:foo, :bar).new('foo-value', 'bar-value') }

    before do
      allow(Reverb::Configuration).to receive(:current).and_return config

      Reverb.configuration do |config_item|
        config_item.foo = 'foo-changed'
        config_item.bar = 'bar-changed'
      end
    end

    it 'yields the configuration' do
      expect(config.foo).to eq 'foo-changed'
      expect(config.bar).to eq 'bar-changed'
    end
  end
end
