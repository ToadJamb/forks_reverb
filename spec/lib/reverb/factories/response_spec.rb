require 'spec_helper'

RSpec.describe "#{Reverb::Response} factory" do
  subject { klass.new response }

  shared_examples 'a pre-defined factory' do
      |factory, response_status, response_success, response_data|
    context "given #{factory}" do
      subject { build factory }

      it "has a status of #{response_status.inspect}" do
        expect(subject.status).to eq response_status
      end

      it "has success value of #{response_success.inspect}" do
        expect(subject.success?).to eq response_success
      end

      it "has a data value of #{response_data.inspect}" do
        expect(subject.data).to eq response_data
      end
    end
  end

  it_behaves_like 'a pre-defined factory',
    :reverb_response, 200, true, 'data-value'
  it_behaves_like 'a pre-defined factory',
    :reverb_server_error_response, 500, false, nil
  it_behaves_like 'a pre-defined factory',
    :reverb_unsuccessful_response, 400, false, nil
  it_behaves_like 'a pre-defined factory',
    :reverb_unprocessable_entity_response, 422, false, nil

  shared_examples 'a factory attribute' do |attribute, value|
    subject { build :reverb_response, attribute => value }
    context "given #{attribute} is set to #{value.inspect}" do
      describe "#{attribute}" do
        it "returns #{value.inspect}" do
          expect(subject.send(attribute)).to eq value
        end
      end
    end
  end

  it_behaves_like 'a factory attribute', :status, 200
  it_behaves_like 'a factory attribute', :success, true
  it_behaves_like 'a factory attribute', :data, 'factory-data'
end
