Gem::Specification.new do |spec|
  spec.name          = 'reverb'
  spec.version       = '0.0.4'
  spec.authors      << 'Travis Herrick'
  spec.authors      << 'Josh Book'
  spec.email         = ['travish@awesomenesstv.com']
  spec.summary       = 'Generic Response Interface'
  spec.description   = '
    A generic interface for responding to API requests.
  '.strip

  spec.homepage      = 'https://github.com/awesomenesstv/reverb'
  spec.license       = 'LGPLv3'
  spec.files         = Dir['lib/**/*.rb', 'license/*']

  spec.extra_rdoc_files = [
    'README.md',
    'license/gplv3.md',
    'license/lgplv3.md',
  ]

  spec.add_development_dependency 'gems',         '~> 0'

  spec.add_development_dependency 'rake_tasks',   '~> 4.1'
  spec.add_development_dependency 'cane',         '~> 2'
  spec.add_development_dependency 'rspec',        '~> 3'
  spec.add_development_dependency 'factory_girl', '~> 4'
  spec.add_development_dependency 'faraday',      '~> 0'
end
