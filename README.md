Reverb
======

[![Build Status](https://semaphoreci.com/api/v1/projects/2ed8a518-6a6c-4ebe-844b-4a1fc310baa4/459078/badge.svg)](https://semaphoreci.com/awesomenesstv/reverb--2)

Reverb provides a generic interface for responding to API requests.


Setup
-----

	$ bundle install


Run Specs
---------

	$ bundle exec rake


Interface
---------

Simply inherit from `Reverb::Response`:

```
class MyResponse < Reverb::Response
  def on_success
    # Process data here.
    self.data = MyClass.new(body['requested-data'])
  end
end
```

Responses expect to be passed a response object,
but do not require them so that they can be
generated with FactoryGirl.

The `success` method is provided purely for the purposes
of factory generation and should not be used for any other use.

The only methods that are considered part of the public API are:

* `success?`
* `status`
* `data`
* `on_success`
* `scrub`
* `scrubbed`

`on_success` only gets called if `success?` is true.

We recommend that the data set in `on_success` is a ruby object
(or array of ruby objects) with specific attributes for that request.

See [brainzz][brainzz] for an example.


Factories
---------

Reverb includes FactoryGirl factories for your convenience.
Include them after requiring FactoryGirl:

	require 'reverb/factories'


Sensitive Data
--------------

Responses may be scrubbed to remove sensitive data such as credentials.
Use `response.scrubbed` to access the 'scrubbed' version of the response.

By default, Reverb will scrub etags.

To turn this off, set `scrub_etags` to false in a configuration block:

```
Reverb.configure do |config|
  config.scrub_etags = false
end
```


To turn off scrubbing entirely, set `scrub` to false a configuration block:
This will cause no scrubbing to occur, even if `scrub_etags` is set to true.

```
Reverb.configure do |config|
  config.scrub = false
end
```


To scrub custom values, simply call `scrub` with the regex/string
to scrub and a replacement string.

```
response = Reverb::Response.new(original_response)
response.scrub 'sensitive-string', 'YOU_CANT_SEE_ME'
```

The code above will replace all occurences
of 'sensitive-string' with 'YOU_CANT_SEE_ME'.



Deployment
----------

This project makes use of branches to manage deployment.
Pushing a new commit to the `production` branch
will also build and push this gem to RubyGems.


[brainzz]: https://www.github.com/awesomenesstv/brainzz
