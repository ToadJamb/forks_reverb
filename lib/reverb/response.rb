module Reverb
  class Response
    # Success is only exposed to facilitate factory generation.
    # It should not be used in code. Please use success? instead.
    attr_accessor :status, :success, :data

    def initialize(response = nil)
      @status    = nil
      @success   = false
      @body      = nil
      @response  = response
      @scrubbers = {}

      process_response if @response
    end

    def success?
      @success
    end

    def scrubbed
      return @scrubbed if defined?(@scrubbed)

      scrub etag, 'ETAG' if Configuration.current.scrub_etag && etag

      scrubbers = @scrubbers
      @scrubbers = {}

      @scrubbed = PP.pp(self, '')

      scrub_all scrubbers

      @scrubbers = scrubbers

      @scrubbed
    end

    def scrub(regex, value)
      return self unless regex
      @scrubbers[regex] = value
      self
    end

    private

    def process_response
      @status = @response.status
      @success = @response.success?

      if success?
        @body = JSON.parse(@response.body)
        on_success
      end
    end

    def scrub_all(scrubbers)
      return unless Configuration.current.scrub
      scrubbers.each do |regex, value|
        wipe regex, value
      end
    end

    def wipe(regex, value)
      value ||= 'NIL_SCRUB'

      regexes(regex).each do |match|
        @scrubbed = @scrubbed.gsub(match, value)
      end
    end

    def regexes(regex)
      return [regex] unless regex.is_a?(String)

      escaped = CGI.escape(regex)

      list = Set.new([regex])
      list << escaped
      list << escaped.gsub('+', '%20').gsub('%2F', '/')

      list
    end

    def etag
      raw_etag &&
        @response.env.response_headers['etag'].gsub(/"/, '')
    end

    def raw_etag
      response_headers &&
        @response.env.response_headers.is_a?(Hash) &&
        @response.env.response_headers['etag'] || nil
    end

    def response_headers
      response_env &&
        @response.env.respond_to?(:response_headers) &&
        @response.env.response_headers || nil
    end

    def response_env
      @response.respond_to?(:env) &&
        @response.env || nil
    end

    def body
      @body
    end

    def on_success
    end
  end
end
