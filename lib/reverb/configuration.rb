module Reverb
  class Configuration
    attr_accessor :scrub, :scrub_etag

    def initialize
      @scrub      = true
      @scrub_etag = true
    end

    class << self
      def current
        @config ||= self.new
      end
    end
  end
end
