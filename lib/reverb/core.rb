module Reverb
  module Core
    def configuration(&block)
      yield Configuration.current
    end
  end

  extend Core
end
