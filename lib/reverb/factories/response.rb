FactoryGirl.define do
  factory :reverb_response, :class => Reverb::Response do
    success true
    status 200
    data 'data-value'

    factory :reverb_unsuccessful_response do
      success false
      status 400
      data nil

      factory :reverb_unprocessable_entity_response do
        status 422
      end

      factory :reverb_server_error_response do
        status 500
      end
    end
  end
end
